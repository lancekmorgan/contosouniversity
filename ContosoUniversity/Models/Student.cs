﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Student
    {
        public const string NoWhiteSpaceRegex = @"^[A-Z]+[a-zA-Z''-'\s]*$";
        public const string NoWhiteSpaceRegexError = @"Your name must be capitalized and no blank spaces are allowed.";

        public int ID { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        [RegularExpression(NoWhiteSpaceRegex, ErrorMessage = NoWhiteSpaceRegexError)]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(NoWhiteSpaceRegex, ErrorMessage = NoWhiteSpaceRegexError)]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Column("FirstName")]
        [DisplayName("First Name")]
        public string FirstMidName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Enrollment Date")]
        public DateTime EnrollmentDate { get; set; }

        [DisplayName("Full Name")]
        public string FullName
        {
            get { return String.Format("{0}, {1}", LastName, FirstMidName); }
        }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}